$(document).ready(function () {

  //menu dropdown toggle
  $('.sidenav .dropdown').click(function () {
    $(this).toggleClass('open');
  });


  $('.user-acc').click(function () {
    $('.dropdown-menu').toggleClass('open');
  });

  //menu link click active
  $('.sidenav .nav-item a').click(function () {
    $(this).parent().addClass('active').siblings().removeClass('active');
  });

  //expanded sidebar

  $('.header__expand-coll').click(function () {
    $('body').toggleClass('mini-sidebar');
  });

  //modal
  $('.changeapi-btn').click(function () {
    $('.changeapi-modal').toggle();
  });

  $('.apikey-btn').click(function () {
    $('.apikey-modal').toggle();
  });

  $('.changepwd-btn').click(function () {
    $('.changepwd-modal').toggle();
  });

  $('.swapac-btn').click(function () {
    $('.swapac-modal').toggle();
  });

  $('.close, .btn-close').click(function () {
    $('.modal').hide();
  });



  $(function() {
    $(".chzn-select").chosen();
})

});